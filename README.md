
WLS_coating
===========

Installing
----------



Data Sources
------------

### p-Terphenyl

Data from http://omlc.org/spectra/PhotochemCAD/html/003.html

### PMTs

 * Photonis XP4318B - Graph take-off from Photonis supplied sensitivity curve.
   ![Alt text](https://bytebucket.org/whit2333/wls_coating/raw/13c913dc750625f78f51437d2d878c652c1aaedb/data/XP4318B_spectral_characteristics.png)


Authors
-------

Whitney Armstrong (whit@temple.edu)
Sylvester Joosten

