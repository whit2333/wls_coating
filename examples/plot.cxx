Int_t plot(){

   gSystem->Load("lib/libWLSCoating.so");

   WLSEmission  * emission = new WLSEmission();
   //WLSEmission emission2("ghostfile.txt");

   WLSAbsorption * abs = new WLSAbsorption();
   //WLSAbsorption ab2("ghostfile2.txt");

   PMTQE   *  pmt   = new PMTQE( "data/XP4500B.dat");
   PMTQE   *  pmt2  = new PMTQE("data/XP4318B.dat");

   TCanvas * c = new TCanvas("WLS_plot","WLS example plot");
   c->Divide(2,2);

   c->cd(1);
   abs->fGraph->Draw();
   abs->fGraph->SetTitle("Absorption");

   c->cd(2);
   TF1 * f = emission.GetFunction();
   //TF1 * f = new TF1("WLSEmissionFunc", emission, 300, 500.0, 0, "WLSEmission");
   f->DrawCopy("l");
   //emission.fGraph->Draw();
   //emission.fFunction->SetTitle("Emission");

   c->cd(3);
   TLegend * leg = new TLegend(0.1, 0.7, 0.4, 0.9);
   pmt2->fGraph->SetTitle("PMT Sensitivity");
   pmt2->fGraph->Draw();
   pmt->fGraph->SetLineColor(2);;
   pmt->fGraph->Draw("same");;
   leg->AddEntry(pmt->fGraph,"XP4500B","l");
   leg->AddEntry(pmt2->fGraph,"XP4318B (SANE)","l");
   leg->Draw();

   return 0;
}
