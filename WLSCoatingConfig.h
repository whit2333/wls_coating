#ifndef WLSCoatingConfig_HH

#define WLSCoating_VERSION_MAJOR 0 
#define WLSCoating_VERSION_MINOR 0 
#define WLSCoating_PREFIX  "~"
#define WLSCoating_CXX_LIBS  "-L~/lib -lWLSCoating"
#define WLSCoating_CXX_LDFLAGS  ""
#define WLSCoating_CXX_INC_DIR  "~/include/WLSCoating"
#define WLSCoating_CXX_CFLAGS   ""

#endif

