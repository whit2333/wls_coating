#include "WLSEmission.h"
#include <iostream>
#include <fstream>
//______________________________________________________________________________
WLSEmission::WLSEmission(const char * fname){
   fNormalization = 1;
   fFunction      = 0;
   fGraph         = 0;
   fXMin          = 0;
   fXMax          = 1;

   std::ifstream file(fname);
   if( file.is_open() ) {
      double xi,yi;
      if( file.good() ) {
         while(!file.eof()) {
            file >> xi >> yi ;
            if(file.eof()) break;
            fX.push_back(xi);
            fY.push_back(yi);
         }
      }
      fXMin  = fX.front();
      fXMax  = fX.back();
      fGraph = new TGraph(fX.size(),&fX[0],&fY[0]);
      file.close();
      // Create a function to calculate the integral 
      //TF1 * f = new TF1("f",fobj,0,1,npar,"MyFunctionObject");
      fFunction      = new TF1("WLSEmissionFunc", this, fXMin, fXMax, 1, "WLSEmission");
      fNormalization = fFunction->Integral(fXMin, fXMax); 
      std::cout << " Norm: " << fNormalization << std::endl;
      delete fFunction;
      fFunction = 0;
   } else {
      Error("WLSEmission", Form("File:%s could not be opened",fname) );
   }
   std::cout << "min: " << fXMin << ", max: " << fXMax << std::endl;
}
//______________________________________________________________________________
WLSEmission::~WLSEmission(){
   //delete fGraph;
}
//______________________________________________________________________________
TF1 * WLSEmission::GetFunction(){
   if(!fFunction) {
      fFunction = new TF1("WLSEmissionFunc", this, fXMin, fXMax, 0, "WLSEmission");
   }
   return fFunction;
}
//______________________________________________________________________________
double WLSEmission::Eval(double x) const {
   if( x>fXMin && x<fXMax )  {
      //std::cout << "x = " << x << std::endl;
      return( fGraph->Eval(x)/fNormalization );
   }
   //std::cout << "x = " << x << std::endl;
   //std::cout << fGraph  << std::endl;
   //if(!fGraph) {
   //   Error("Eval","No graph data set");
   //   return 0.0;
   //}
   return 0.0;
}
//______________________________________________________________________________
double WLSEmission::operator()(double *x,double *p){
   return( Eval(x[0]) );
}
//______________________________________________________________________________

