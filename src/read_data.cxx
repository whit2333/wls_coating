TGraph * read_data(){

   // File should have two space separated values
   const char * filename = "003-ems.dat";
   ifstream file(filename);

   double xi,yi;

   std::vector<double> x;
   std::vector<double> y;

   if( file.good() ) {
      while(!file.eof()) {
         file >> xi >> yi ;
         if(file.eof()) break;
         x.push_back(xi);
         y.push_back(yi);
      }
   }

   TGraph * gr = new TGraph(x.size(),&x[0],&y[0]);

   return gr;
}
