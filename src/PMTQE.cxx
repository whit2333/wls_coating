#include "PMTQE.h"
#include <iostream>
#include <fstream>
//______________________________________________________________________________
PMTQE::PMTQE(const char * fname){
   std::ifstream file(fname);

   if( file.is_open() ) {
      double xi,yi;
      if( file.good() ) {
         while(!file.eof()) {
            file >> xi >> yi ;
            if(file.eof()) break;
            fX.push_back(xi);
            fY.push_back(yi);
         }
      }
      fXMin = fX.front();
      fXMax = fX.back();
      fGraph = new TGraph(fX.size(),&fX[0],&fY[0]);
      file.close();
   } else {
      Error("WLSAbsorption",Form("File:%s could not be opened",fname));
      fXMin  = 0;
      fXMax  = 1;
      fGraph = 0;
   }

}
//______________________________________________________________________________
PMTQE::~PMTQE(){
   //delete fGraph;
}
//______________________________________________________________________________
double PMTQE::Eval(double x) const {
   if(!fGraph) {
      Error("Eval","No graph data set");
      return 0.0;
   }
   if( x>fXMin && x<fXMax ) 
      return fGraph->Eval(x);
   return 0.0;
}
//______________________________________________________________________________

