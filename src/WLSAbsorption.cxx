#include "WLSAbsorption.h"
#include <iostream>
#include <fstream>

//______________________________________________________________________________
WLSAbsorption::WLSAbsorption(const char * fname){
   std::ifstream file(fname);
   if( file.is_open() ) {
      double xi,yi;
      if( file.good() ) {
         while(!file.eof()) {
            file >> xi >> yi ;
            if(file.eof()) break;
            fX.push_back(xi);
            fY.push_back(yi);
         }
      }
      fXMin  = fX.front();
      fXMax  = fX.back();
      fGraph = new TGraph(fX.size(),&fX[0],&fY[0]);
      file.close();
      std::cout << fX.size() << " data points. " << std::endl;
   } else {
      Error("WLSAbsorption",Form("File:%s could not be opened",fname));
      fXMin  = 0;
      fXMax  = 1;
      fGraph = 0;
   }

}
//______________________________________________________________________________
WLSAbsorption::~WLSAbsorption(){
   //delete fGraph;
}
//______________________________________________________________________________
double WLSAbsorption::Eval(double x) const {
   if(!fGraph) {
      Error("Eval","No graph data set");
      return 0.0;
   }
   if( x>fXMin && x<fXMax ) 
      return fGraph->Eval(x);
   return 0.0;
}
//______________________________________________________________________________

