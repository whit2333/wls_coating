#ifndef WLSAbsorption_HH
#define WLSAbsorption_HH 1

#include <vector>
#include "TObject.h"
#include "TGraph.h"

class WLSAbsorption : public TObject {

   protected:
      std::vector<double> fX;
      std::vector<double> fY;

   public:
      WLSAbsorption(const char * fname = "data/003-abs.dat");
      ~WLSAbsorption();

      double Eval(double x) const ;

      TGraph * fGraph;
      double   fXMin;
      double   fXMax;

      ClassDef(WLSAbsorption,1)
};

#endif

