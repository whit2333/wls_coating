#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ all typedef;


#pragma link C++ class WLSAbsorption+;
#pragma link C++ class WLSEmission+;
#pragma link C++ class PMTQE+;


#endif
