#ifndef PMTQE_HH
#define PMTQE_HH 1

#include "TGraph.h"
#include "TObject.h"
#include <vector>

class PMTQE : public TObject {

   protected:
      std::vector<double> fX;
      std::vector<double> fY;

   public:
      PMTQE(const char * fname = "data/XP4318B_spectral_characteristics.dat");
      ~PMTQE();

      double Eval(double x) const ;

      TGraph * fGraph;
      double   fXMin;
      double   fXMax;

      ClassDef(PMTQE,1)
};

#endif

