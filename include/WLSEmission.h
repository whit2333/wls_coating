#ifndef WLSEmission_HH
#define WLSEmission_HH 1

#include "TObject.h"
#include "TGraph.h"
#include "TF1.h"
#include <vector>

class WLSEmission : public TObject {

   protected:
      std::vector<double> fX;
      std::vector<double> fY;

   public:
      WLSEmission(const char * fname = "data/003-ems.dat");
      ~WLSEmission();

      double Eval(double x) const ;
      double operator()(double *x,double *p);

      TGraph * fGraph;
      TF1    * fFunction;

      TF1 *  GetFunction();

      double fXMin;
      double fXMax;
      double fNormalization;

      ClassDef(WLSEmission,1)
};

#endif

